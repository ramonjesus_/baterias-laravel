
    @extends('templates.base')

    @section('conteudo')
        
   <main>
        <h1><em>Medições</em></h1>
        <hr>
        <h2>Valores obtidos:</h2>
        <table class="table table-striped table-bordered" id="tbDados">
            <tr>
                <th>Pilha/Bateria</th>
                <th>Tensão nominal (v)</th>
                <th>Capacidade de corrente</th>
                <th>Tensão sem carga (v)</th>
                <th>Tensão com carga (v)</th>
                <th>Resistênsia de carga (ohm)</th>
                <th>Resistência interna (ohm)</th>
            </tr>
            <tr>
                <td>
                   Pilha Alcalina Duracel AA </td>
                <td>1,5 </td>
                <td>2800mA.</td>
                <td>1,384</td>
                <td>1,286</td>
                <td>23,7</td>
                <td>0,3317</td>
            </tr>
        </table>
    </main>
   
    @endsection
    @section('rodape')
    <h4>Rodapé da página principal</h4>
    @endsection
 