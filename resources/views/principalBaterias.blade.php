
    @extends('templates.base')

    @section('conteudo')
        
    

    <main>
        <h1>Turma 2D3 - Grupo 3</h1>
        <h2>Participantes</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072527</td>
                <td>Henrique de Matos</td>
                <td>Desenvolvedor CSS/HTML</td>
            </tr>
            <tr>
                <td>0072543</td>
                <td>Júlio Cézar</td>
                <td>Gerente</td>
            </tr>
            <tr>
                <td>0072538</td>
                <td>Lucas Pedro </td>
                <td>Medição das Baterias</td>
            </tr>
            <tr>
                <td>0072561</td>
                <td>Matheus Machado </td>
                <td>Medição das Baterias</td>
            </tr>
            <tr>
                <td>0073008</td>
                <td>Ramos de Jesus</td>
                <td>Desenvolvedor JavaScript / Design</td>
            </tr>
        </table>
        <img src="imgs/estudantes.jpg" width="60%" alt="Integrantes do Grupo">
    </main>
    @endsection
    @section('rodape')
    <h4>Rodapé da página principal</h4>
    @endsection
 