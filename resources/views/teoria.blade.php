
    
    @extends('templates.base')

    @section('conteudo')
        
    <main>
        <h1><em>Teoria</em></h1>
        
        <hr>
        
        <h2>Pilhas:</h2>
        <p>
            Pilhas são dispositivos capazes de produzir corrente elétrica (energia elétrica) a partir de reações de oxidação e redução de componentes metálicos presentes em sua estrutura. Vale dizer que:  
            <p>- Oxidação: é a capacidade que um material apresenta de perder elétrons;</p>  
            <p>- Redução: é a capacidade que um material apresenta de ganhar elétrons.</p>  
            Assim, em uma pilha, como os elétrons partem de um componente e chegam até outro, forma-se uma corrente elétrica, que é capaz de fazer funcionar diversos dispositivos eletrônicos.
        </p>
        
        <img src="/imgs/pilhas.jpg" width="15%" alt="pilhas"> 
        
        <hr>
        
        <h2>Baterias:</h2>
        <p>Baterias são dispositivos que produzem corrente elétrica a partir de reações de oxidorredução reversíveis, ou seja, que podem ser renovadas e continuar gerando energia.</p>
        <p>As baterias são conjuntos de pilhas ligadas em série, ou seja, são dispositivos eletroquímicos nos quais ocorrem reações de oxidorredução, produzindo uma corrente elétrica. Podem ser chamadas ainda de pilhas secundárias, baterias secundárias ou acumuladores.</p>   
        <img src="/imgs/bateria.jpeg" width="370px" alt="baterias"> 
    </main>
    
    @endsection
    @section('rodape')
    <h4>Rodapé da página principal</h4>
    @endsection
 