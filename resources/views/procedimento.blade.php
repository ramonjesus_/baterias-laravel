
 
    @extends('templates.base')

    @section('conteudo')
        
    <main>
        <h1><em>Procedimentos</em></h1>
        <hr>
        <h2>Materiais:</h2>
        <p>
            Para efetuarmos as medições foi necessário a utilização dos seguintes materiais:
        
            <p>- 1 Multímetro;</p>
            <img src="/imgs/multímetro.jpg" width="10.7%" alt="multímetro">
            <p><em>Imagem ilustrativa</em></p>
            <hr>
            <p>- 1 Resistor;</p>
            <img src="/imgs/resistor.jpg" width="15%" alt="resistor">
            <p><em>Imagem ilustrativa</em></p>
            <hr>
            <p>- 1 Caderno para anotações;</p>
            <img src="/imgs/caderno.jpg" width="13%" alt="caderno">
            <p><em>Imagem ilustrativa</em></p>
            <hr>
            <p>- Pilhas/Baterias;</p>
            <img src="/imgs/pilhasilustração.jpeg" width="13%" height="150px" alt="pilhas">
            <p><em>Imagem ilustrativa</em></p>
            
            <hr>
            <h2>Mão na Massa:</h2>
            <p>- Inicializamos a parte prática do projeto, medindo a resistência do resistor que vai ser utilizado; anotamos e partimos para os outros componentes
                (baterias). 
            <p>- Definimos com Tensão(v) a medida de tensão das baterias sem carga (resistor), usando somente o multímetro;</p>
            <p>- Tensão(e) a medida com carga, usando além do multímetro o resistor conectado nas pontas de prova para sabermos as quedas de tensões de cada componente.</p>
            </p>
        </p>
    </main>
    @endsection
    @section('rodape')
    <h4>Rodapé da página principal</h4>
    @endsection
 