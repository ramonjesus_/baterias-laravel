@extends('templates.base')

@section('conteudo')
    
    <main>

        <h1><em>Conclusão</em></h1>
        <hr>
        <p>Através da realização das medições, podemos compreender como a resistência interna das baterias afeta a tensão disponível. 
        <p>Observamos como o uso de um resistor auxilia na determinação dessa resistência, permitindo uma análise mais precisa do comportamento desses dispositivos em diferentes situações. Essa compreensão é fundamental para a aplicação adequada de baterias e pilhas em diversas situações.</p>
        <p>A tabela informando cada medida é uma excelente forma de apresentar os dados obtidos de maneira organizada e de fácil entendimento. Com esses resultados, identificamos possíveis variações nas medições e debater sobre o que influência essas diferenças.</p>
        <p>Neste trabalho, conseguimos demonstrar o conhecimento em conceitos importantes de eletricidade, química e resistência elétrica, além de adquirirmos habilidades práticas em medições e experimentação. Essas habilidades serão essenciais para futuros estudos e aplicações no campo da elétrica e eletrônica.</p>
        <p>Ao realizar a atividade e discutir os resultados em grupo, nós compartilhamos ideias, superamos desafios e aprendemos juntos, enriquecendo a experiência e o aprendizado.</p>
        <p><em><b>Obrigado pelo seu acesso!</b></em></p>    
    </main>
    @endsection
    @section('rodape')
    <h4>Rodapé da página principal</h4>
    @endsection
 